package cn.stylefeng.roses.kernel.dict.modular.mapper;

import cn.stylefeng.roses.kernel.dict.modular.entity.Dict;
import cn.stylefeng.roses.kernel.dict.modular.model.DictInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 基础字典 Mapper 接口
 * </p>
 *
 * @author fengshuonan
 * @since 2018-07-24
 */
public interface DictMapper extends BaseMapper<Dict> {

    /**
     * 获取字典列表
     *
     * @author fengshuonan
     * @Date 2018/7/25 下午5:21
     */
    List<DictInfo> getDictList(Page page, @Param("dictInfo") DictInfo dictInfo);

    /**
     * 获取字典列表
     *
     * @author fengshuonan
     * @Date 2018/7/25 下午5:21
     */
    List<DictInfo> getDictList(@Param("dictInfo") DictInfo dictInfo);

    /**
     * 根据字典类型code和父id获取下级字典
     *
     * @author fengshuonan
     * @data 2018/9/17 19:06
     */
    List<DictInfo> getDictListByTypeCodeAndPid(Map<String, Object> map);

    /**
     * 根据字典类型code和非父id获取下级字典
     *
     * @author fengshuonan
     * @Date 2019年4月1日
     */
    List<DictInfo> getDictListByTypeCodeAndNotPid(Map<String, Object> map);

    /**
     * 翻译Code对应的字典名称
     *
     * @author fengshuonan
     * @date 2019/4/1
     **/
    List<String> getDictName(String dictCode);

    /**
     * 根据字典类型code和字典名称获取字典码
     *
     * @param dictTypeCode 字典类型code
     * @param dictName     字典名称
     * @author fengshuonan
     * @date 2019/4/1
     **/
    List<String> getDictCode(@Param("dictTypeCode") String dictTypeCode, @Param("dictName") String dictName);
}
