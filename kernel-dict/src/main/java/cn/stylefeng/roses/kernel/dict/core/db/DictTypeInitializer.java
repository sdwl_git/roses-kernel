package cn.stylefeng.roses.kernel.dict.core.db;


import cn.stylefeng.roses.core.db.DbInitializer;
import cn.stylefeng.roses.kernel.dict.modular.entity.DictType;
import org.springframework.stereotype.Component;

/**
 * 字典类型表的初始化程序
 *
 * @author fengshuonan
 * @date 2018-07-30-上午9:29
 */
@Component
public class DictTypeInitializer extends DbInitializer {

    @Override
    public String getTableInitSql() {
        return "CREATE TABLE `cbd_sys_dict_type` (\n" +
                "  `dict_type_id` varchar(36) NOT NULL COMMENT '字典类型id',\n" +
                "  `dict_type_class` smallint(1) DEFAULT NULL COMMENT '类型1：业务类型2：系统类型',\n" +
                "  `dict_type_code` varchar(255) NOT NULL COMMENT '字典类型编码',\n" +
                "  `dict_type_name` varchar(255) NOT NULL COMMENT '字典类型名称',\n" +
                "  `dict_type_desc` varchar(1000) DEFAULT NULL COMMENT '字典描述',\n" +
                "  `status` smallint(11) NOT NULL DEFAULT '1' COMMENT '状态1：启用2：禁用',\n" +
                "  `create_time` datetime DEFAULT NULL COMMENT '添加时间',\n" +
                "  `update_time` datetime DEFAULT NULL COMMENT '修改时间',\n" +
                "  `dict_type_sort` double(11,5) DEFAULT NULL COMMENT '排序',\n" +
                "  `create_user` bigint(11) DEFAULT NULL,\n" +
                "  `update_user` bigint(11) DEFAULT NULL,\n" +
                "  `del_flag` varchar(255) DEFAULT NULL,\n" +
                "  PRIMARY KEY (`dict_type_id`) USING BTREE\n" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='字典类型表'";
    }

    @Override
    public String getTableName() {
        return "cbd_sys_dict_type";
    }

    @Override
    public Class<?> getEntityClass() {
        return DictType.class;
    }
}
